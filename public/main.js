var Base = require('base/Base');
var BaseRoutes = require('base/Routes');
var Auth = require('auth/Auth');
var AuthRoutes = require('auth/Routes');
var Demo = require('demo/Demo');
var DemoRoutes = require('demo/Routes');

Base.requires.push('Auth');
Base.requires.push('Demo');

Base.initialize(function() {
    BaseRoutes();
    AuthRoutes();
    DemoRoutes();
});
